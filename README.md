# Pixiv Downloader #

### Target Users ###

* Machine Learning Enthusiasts (esp. in Computer Vision)
* Data Enthusiasts
* Funny/Cute Illustrations Lovers

### What is this repository for? ###

* This is a handy python library for collecting illustrations and corresponding meta-data from Pixiv, a SNS for illustrations.
* Those collected data consist of a large number of labeled images, which means we can apply machine learning techniques and statistical analysis on it. It is especially suitable for deep learning where a gigantic database is preferred.
* The main purpose of this project is to simplify the steps to take for collecting data from Pixiv.
* For most simplest setting, you can just install this module, type your account info and run it in a terminal. With the default setting, the program will keep collecting data for you without burdening on Pixiv's servers.

### Installation ###
Requirements:

* Python 3.5
* PixivPy (https://github.com/upbit/pixivpy/tree/master/pixivpy3)
* TinyDB (https://github.com/msiemens/tinydb)

Optional:

* tqdm (https://pypi.python.org/pypi/tqdm)
* ujson (https://pypi.python.org/pypi/ujson) (Windows Binary => http://www.lfd.uci.edu/~gohlke/pythonlibs/)
* colorama (https://pypi.python.org/pypi/colorama)

These libraries can be installed via pip.

### Usage ###

This library already contains a driver program for you.

* Open the terminal and type


```
#!python

python task.py -t download -f
```

to download works from artists who are in monthly ranking.

On average, it collects 10k images (~= 8 GB) each day. Please make sure you have enough data storage & network cap.

To download works from more artists,

```
#!python

python task.py -t update -d 1
```

to update artists' database by exploring users followed by artists who are in monthly ranking.

You can then download all the works of those artists by using a command shown above.

Of course you can write your own program to collect data for specific domain using this library, but make sure that you have enough time of thread-sleep between iterative operations as we do not want to "attack" on the servers.

Main components are:

* pixivdldr.py

responsible for managing PixivAPIs to access their servers. There are many handy functions that work for you.

* pixivdb.py

responsible for storing meta-data in json file using TinyDB. Default json parser for python is very slow (which is good for Pixiv servers!). If you want to make it faster, consider using ujson (https://github.com/esnme/ultrajson/blob/master/python/ujson.c)

### TODO ###

More features will be implemented in near future, including

* Data processing tools to tailor the stored data for your desired format, such as gathering images with specific tags into another folder, or trim, convert meta-data in specific format
* Data analysis toolkits, most likely by using sci-kit learn and matplotlib.

### License ###

MIT License