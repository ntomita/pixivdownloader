from pixivdldr import PixivDownloader as PD
import sys
import os
import argparse


def main(argv=sys.argv[1:]):
    """Sample Command
    > python task_single -id 4545306

    Specify from which artist you'd like to obtain artworks
        using id flag.
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("-id", type=int, required=True)
    args = parser.parse_args(argv)

    pd = PD('client.json')
    pd.folder_name = "pixiv_indiv"
    pd.db_name = os.path.join(os.getcwd(), "db_indiv", "pixivdb.json")
    pd.download_all_works(args.id)

if __name__ == '__main__':
    main()