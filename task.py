from pixivdldr import PixivDownloader
from tqdmcounter import TqdmCounter
import sys
import os
import argparse
try:
    from tqdm import tqdm
    tqdm_enabled = True
except:
    tqdm_enabled = False


def main(argv=sys.argv[1:]):
    """Sample Commands
    1.
    > python task.py -t update
    > python task.py -t update -d 1

    Update json database containing artists' id
    by using ranking data (, let's call these artists A(0)).
    Set Depth parameter d >= 0 if you also want to check artists A(1)
        who are followed by A(0).
    In general, you can explore artists A(d) who are followed by A(d-1).
    As you set d larger, it covers more artists and computation/network costs
        explodes. Just be careful.

    2.
    > python task.py -t download -m monthly -f
    Download images and meta-data from Pixiv and store them
        under a specific folder.
    Set mode defines where (or which ranking) you'd like to refer as a source
        of a list of artists.
    If you set force flag, it checks new artworks from previously explored
        artists. You would need to set this flag after you performed update task.


    Available Arguments:
        Task(t): [update, download]
        Mode(m): [daily, weekly, monthly, rookie, daily_r18, weekly_r18, r18g]
            Default: monthly
        Depth(d): [0..n]
            Default: 0
        Force(f): true/false
            Default: false
    """

    parser = argparse.ArgumentParser()
    parser.add_argument("-t", type=str, required=True)
    parser.add_argument("-d", type=int, default=0)
    parser.add_argument("-m", type=str, default="monthly")
    parser.add_argument("-f", action="store_true")
    args = parser.parse_args(argv)

    pd = PixivDownloader('client.json', tqdm_enabled=tqdm_enabled)
    artist_file_path = os.path.join(os.getcwd(), "tmp", "artists.p")
    saved_artists = PixivDownloader.load(artist_file_path)
    exclude_file_path = os.path.join(os.getcwd(), "tmp", "exclude.p")
    exclude = PixivDownloader.load(exclude_file_path)

    if args.t == 'update':
        artists = pd.get_artists_from_ranking(mode=args.m)
        if args.d > 0:
            counter = TqdmCounter(tqdm_enabled=tqdm_enabled,
                                  total=len(artists),
                                  desc="Update")
            for artist in artists:
                saved_artists = pd.get_following_of_artist_rec(artist,
                                                                saved_artists,
                                                                args.d)
                PixivDownloader.save(
                   list(set(saved_artists)),
                    artist_file_path)
                counter.update(1)
                
            PixivDownloader.save(list(set(saved_artists)),artist_file_path)
            # PixivDownloader.save(
            #     pd.sort_artists(list(set(saved_artists))),
            #     artist_file_path)
            counter.close()
        else:
            PixivDownloader.save(list(set(saved_artists + artists)),artist_file_path)
            # PixivDownloader.save(
            #     pd.sort_artists(
            #         list(set(saved_artists + artists))),
            #     artist_file_path)
    elif args.t == 'download':
        if saved_artists == []:
            saved_artists = pd.get_artists_from_ranking(mode=args.m)
            PixivDownloader.save(saved_artists, artist_file_path)
            # PixivDownloader.save(
            #     pd.sort_artists(saved_artists),
            #     artist_file_path)
        counter = TqdmCounter(tqdm_enabled=tqdm_enabled,
                              total=len(saved_artists),
                              desc="DL (Total)")
        for artist in saved_artists:
            if args.f is False and artist in exclude:
                continue
            pd.download_all_works(artist)
            exclude.append(artist)
            PixivDownloader.save(
                list(set(exclude)),
                exclude_file_path)
            counter.update(1)
        counter.close()

if __name__ == '__main__':
    main()