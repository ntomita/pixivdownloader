from tinydb import TinyDB, Query, where
from tinydb.storages import JSONStorage
from tinydb.middlewares import CachingMiddleware
from collections import defaultdict, Counter
import os

try:
    import ujson as json
except:
    import json

class PixivDB:
    """A class to manage json data related to stored images
    """
    def __init__(self, db_name):
        """
        Args:
            db_name (string): path to a database
        """
        self.db_name = db_name
        self.db = None
        self.query = Query()
        self.reopen(db_name)

    def reopen(self, db_name):
        """Connects to a database
        Args:
            db_name (string): path to a database
        Returns:
            boolean, telling if connection to the database is established
        """
        if not self.is_available():
            self.db = TinyDB(self.db_name, storage=CachingMiddleware(JSONStorage))
            return True
        return False

    def close(self):
        """Close a connection to a database
        """
        if self.is_available():
            self.db.close()
            self.db = None

    def is_available(self):
        """Check if a database is connected
        Returns:
            boolean: True if a database is connected
        """
        if self.db is not None:
            return True
        else:
            # print("Database is closed")
            return False

    def insert(self, data):
        """Insert data into database
        Args:
            data (json string)
        Returns:
            boolean: True if data has successfully inserted.
        """
        if not self.is_available():
            return False
        if self.db.contains(self.query.id == data.id):
            self.update(data)
        else:
            self.db.insert(data)
        return True

    def remove(self, data):
        if self.db.contains(self.query.id == data.id):
            self.db.remove(self.query.id == data.id)
            return True
        return False

    def insert_multiple(self, list_data):
        if not self.is_available():
            return False
        self.db.insert_multiple([data for data in list_data if not self.db.contains(self.query.id == data.id)])
        return True

    def update(self, data):
        if not self.is_available():
            return False
        self.db.update(data, self.query.id == data.id)
        return True

    def search_wid(self, work_id):
        """Return one result or None
        """
        if not self.is_available():
            return None
        return self.db.get(self.query.id == work_id)

    def search_uid(self, user_id):
        """Return possibly several results
        """
        if not self.is_available():
            return None
        return self.db.search(self.query.user.id == user_id)

    def search_tag(self, tag_name):
        if not self.is_available():
            return None
        return self.db.search(where('tags').test(lambda s: tag_name in s))

    def get_tags(self):
        """Returns all tags with total count in Counter object
        Returns:
            collection.Counter which is dictionary with tag names are keys and
                total used count as values 
        """
        with open(self.db_name, encoding='utf-8') as f:
            data = json.load(f)
            tags = []
            for key in data['_default'].keys():
                tags = tags + data['_default'][key]['tags']
            return Counter(tags)

    def get_rating_of(self, work_id):
        if not self.is_available():
            return None
        work = self.search_wid(work_id)
        if work is not None and len(work) > 0:
            return 1.0*work['stats']['score'] / work['stats']['scored_count']
        else:
            return None

    def get_user_score(self, user_id):
        """Returns total score rated on all works of a user
        Args:
            user_id (int)
        Returns:
            int, representing a total score
        """
        works = self.search_uid(user_id)
        return sum([w['stats']['score'] for w in works])

    def get_users_score(self, list_user_id, raw=True):
        """Returns total score rated on all works of each user in a list
        Args:
            list_user_id (list of int)
            raw (boolean): True to enable raw json access that is extremely
                faster than operations on tinydb
        Returns:
            list of tuples, each containing score and user_id
        
        """
        if raw:
            with open(self.db_name, encoding='utf-8') as f:
                data = json.load(f)
                accum_score = defaultdict(int)
                for key in data['_default'].keys():
                    _id = data['_default'][key]['user']['id']
                    accum_score[_id] += data['_default'][key]['stats']['score']
                return [(accum_score.get(user_id, 0), user_id) for user_id in list_user_id]
        else:
            return [(self.get_user_score(user_id), user_id) for user_id in list_user_id ]

    @staticmethod
    def get_path(data, simple=True):
        """Returns a path to an image data
        Args:
            data (json string): json data for a work
        Returns:
            string of a path to a specific image 
        """
        return os.path.join(data.user.id, os.path.basename(data.image_urls.large))


