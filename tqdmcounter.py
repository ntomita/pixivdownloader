import platform

class TqdmCounter:
    """Counter integrated with tqdm (if available)
    """
    def __init__(self, tqdm_enabled=False, total=100, desc=None):
        self.tqdm = None
        self.count = 0
        self.total = total
        if tqdm_enabled:
            from tqdm import tqdm
            self.tqdm = tqdm(total=total, desc=desc)
            if platform.system() == "Windows":
                try:
                    import colorama
                except:
                    print("Please consider using colorama to make tqdm work properly.")

    def update(self, delta):
        if self.tqdm is not None:
            self.tqdm.update(delta)
        self.count += delta

    def __iadd__(self, addend):
        self.update(addend)
        return self

    def close(self):
        if self.tqdm is not None:
            self.tqdm.close()

    def isEnd(self):
        return self.count >= self.total