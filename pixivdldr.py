from pixivpy3 import *
import json
from time import sleep, time
import os
import sys
import pickle
import io
from pixivdb import PixivDB as PDB
from tqdmcounter import TqdmCounter
from random import seed, random
import platform

import logging

class Log:
    def __init__(self, filename):
        timestamp = str(int(time()))
        logging.basicConfig(
            filename=filename+"_"+timestamp+".log", 
            format='%(asctime)s.%(msecs)03d %(levelname)s {%(module)s} [%(funcName)s] %(message)s', 
            datefmt='%Y-%m-%d,%H:%M:%S', 
            level=logging.DEBUG)

    def debug(self, text):
        logging.debug(text)

    def info(self, text):
        logging.info(text)



class PixivDownloader:
    """A class to manage apis for accessing Pixiv

    """
    def __init__(self, credential, tqdm_enabled=False):
        self.credential = credential
        self.__init_apis()
        self.sleep_time_short = 1
        self.sleep_time = 3
        self.sleep_time_long = 60
        self.random_sleep = 0.01
        self.folder_name = "pixiv_images"
        self.db_name = os.path.join(os.getcwd(), "db", "pixivdb.json")
        seed(time())
        self.log = Log(os.path.join(os.getcwd(), "tmp", "log"))
        self.tqdm = None
        if tqdm_enabled:
            from tqdm import tqdm
            self.tqdm = tqdm
            if platform.system() == "Windows":
                try:
                    import colorama
                except:
                    self.print("Please consider using colorama to make tqdm work properly.")

    def __open_db(self):
        """Connect to local json database
        (Private)

        Returns:
            TinyDB object
        """

        dirname = os.path.dirname(self.db_name)
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        pdb = PDB(self.db_name)
        return pdb

    @staticmethod
    def save(list_artists, file_name):
        """Save a list of artists using pickle

        Args:
            list_artists (list of int): each value represents an artist's id
            file_name (string): where to store the list
        """

        dirname = os.path.dirname(file_name)
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        with open(file_name, 'wb') as f:
            pickle.dump(list_artists, f)

    @staticmethod
    def load(file_name):
        """Load a list of artists using pickle

        Args:
            file_name (string): where the pickled list is

        Returns:
            list of int as artists' id
        """

        if not os.path.isfile(file_name):
            return []
        with open(file_name, 'rb') as f:
            return pickle.load(f)

    def tqdm_iter(iterable, desc=""):
        if self.tqdm is not None:
            return self.tqdm(iterable)
        return iterable

    def print(self, obj):
        if self.tqdm is not None:
            self.tqdm.write(str(obj))
        else:
            print(obj)

    def print_separator(self):
        """Wrapper to print a line nicely
        """
        self.print("-"*50)

    def print_header(self, name, num_work):
        """Wrapper to print artist's info nicely

        Args:
            name (string): usually id. User's name may cause unicode related issue.
            num_work (int): total number of artworks
        """
        self.print_separator()
        self.print('Artist: %s' % name)
        self.print('Works: %d' % num_work)
        self.print_separator()

    @staticmethod
    def make_dir(saving_directory_path):
        """Wapper to create a directory

        Args:
            saving_directory_path (string): where to save a file
        """
        if not os.path.exists(saving_directory_path):
            os.makedirs(saving_directory_path)

    def __random_sleep(self):
        """Have a long sleep with a certain probability 
        (Private)
        """
        if random() < self.random_sleep:
            sleep(self.sleep_time_long)
            self.__init_apis()

    def sort_artists(self, list_artists, descending=True):
        """Sort the list of users with respect to their total score obtained.
        Args:
            list_artists (list of int)
            descending (boolean): True for highest to lowest
        Returns:
            list of tuple containing score (int) and user's id (int)
        """
        pdb = self.__open_db()
        score_artist = pdb.get_users_score(list_artists, raw=True)
        score_artist.sort(reverse=descending==True)
        print(score_artist)
        return [pair[1] for pair in score_artist]

    def __login(self):
        """Enable PixivAPI by using an user's credential

        Returns:
            PixivAPI, or None
        """
        self.api = PixivAPI()
        f = open(self.credential, 'r')
        client_info = json.load(f)
        f.close()
        try:
            self.api.login(client_info['pixiv_id'], client_info['password'])
        except PixivError:
            self.print("Make sure you have internet access.")
            return None
        return self.api

    def __init_app_api(self):
        """Enable Pixiv Application API

        Returns:
            Pixiv Application API
        """
        self.aapi = AppPixivAPI()
        return self.aapi

    def __init_apis(self):
        """Enable both Pixiv API and Application API
        """
        self.__login()
        self.__init_app_api()

    def __secure_users_following(self, artist_id, page=1, max_retry=2):
        """Wrapper for api's users_following with redirecting to return
        a response object without error
        """
        for t in range(max_retry):
            result = self.api.users_following(artist_id, page)
            if result.status == "failure":
                if result.errors.system.message == 404:
                    result = None
                    break
                else:
                    result = None
                    sleep(self.sleep_time_long)
                    self.__login()
            else:
                break
        return result

    def get_following_of_artist(self, artist_id):
        """Obtain a list of artists who are followed by an artist

        Args:
            artist_id (int): artist's id who are following others

        Retuns:
            a list of int as artists' id
        """
        result = self.__secure_users_following(artist_id)
        total_followings = result.pagination.total
        num_pages = result.pagination.pages
        users = []
        if total_followings == 0:
            return users
        counter = TqdmCounter(tqdm_enabled=self.tqdm != None,
                              total=total_followings,
                              desc="FLLW ({})".format(artist_id))
        for page in range(1, num_pages+1):
            if page != 1:
                result = self.__secure_users_following(artist_id, page)
                if result is None:
                    break
            for i in range(result.count):
                if counter.isEnd():
                    break
                user_id = result.response[i].id
                users.append(user_id)
                counter.update(1)
        counter.close()
        return users

    def get_following_of_artist_rec(self, root_artist_id, explored=[], depth=1):
        """Recursive version of get_following_of_artist function
        Technically much expressive as it can have a "black list"

        Args:
            root_artist_id (int): artist's id who are following others
            explored (list of int): containing artists' id that will be ignored in
                the process of exploration
            depth (int): degree of depth (refer DFS)

        Retuns:
            a list of int as artists' id
        """
        frontier = [root_artist_id]
        for d in range(depth):
            temp_frontier = []
            counter = TqdmCounter(tqdm_enabled=self.tqdm != None,
                              total=len(frontier),
                              desc="FLLW D({})".format(d+1))
            for user in frontier:
                users = self.get_following_of_artist(user)
                sleep(self.sleep_time_short)
                temp_frontier += [u for u in users
                                  if (u not in explored) and\
                                    (u not in frontier) and\
                                    (u not in temp_frontier)]
                counter.update(1)
            explored += frontier
            frontier = temp_frontier
            counter.close()
        return explored+frontier

    def __secure_ranking(self, ranking_type='illust', mode='monthly', page=1, max_retry=2):
        """Obtain IDs of artists who are currently on a page of ranking
            with reconnect-capability
        (Private)
    
        Args:
            ranking_type (string): which type of ranking you refer
            mode (string): from which ranking to obtain a list of artists.
                [daily, weekly, monthly, rookie, daily_r18, weekly_r18, r18g]
            page (int): from which page to obtain. Please refer to pagination
                attribute in a response object.
        Returns:
            a response object containing all meta-data regarding the page of ranking
        """
        for t in range(max_retry):
            ranking = self.api.ranking(ranking_type=ranking_type,
                                       mode=mode,
                                       page=page)
            if ranking.status == 'failure':
                if ranking.errors.system.code == 404:  # no page exists
                    ranking = None
                    break
                elif ranking.errors.system.message == '400 Bad Request':
                    # no page exists or temporal limiting?
                    ranking = None
                    break
                else:
                    ranking = None
                    sleep(self.sleep_time_long)
                    self.__login()
            else:
                break
        return ranking

    def get_artists_from_ranking(self, max_artists=1000, mode='monthly'):
        """Obtain IDs of artists who are currently on ranking

        Args:
            max_artists (int): Maximum limit for the number of artists
            mode (string): from which ranking to obtain a list of artists.
                [daily, weekly, monthly, rookie, daily_r18, weekly_r18, r18g]

        Returns:
            list of int as artists' id
        """
        per_page = 50
        artists = []
        for page in range(1, int(max_artists/per_page)+1):
            ranking = self.__secure_ranking(mode=mode,
                                            page=page)
            if ranking is None:
                break
            for i in range(len(ranking.response[0].works)):
                artist_id = ranking.response[0].works[i].work.user.id
                artists.append(artist_id)
        return list(set(artists))

    def __secure_users_works(self, artist_id, page=1, max_retry=2):
        """Obtain meta-data of an artist with reconnect-capability

        Args:
            artist_id (int): artist's id
            page (int): from which page to obtain. Please refer to pagination
                attribute in a response object.
        Returns:
            list of json objects containing meta-data for each artwork. None
                when failed.
        """
        for t in range(max_retry):
            try:
                result = self.api.users_works(artist_id, page)
                if result.status == 'failure':
                    if result.errors.system.message == 404:
                        result = None
                        break
                    elif result.errors.system.code == 971:
                        result = None
                        break
                    else:
                        result = None
                        sleep(self.sleep_time_long)
                        self.__init_apis()
                else:
                    break
            except (PixivError, AttributeError) as error:
                self.log.debug(
                    "__secure_users_works {} {} {}".format(artist_id, page, error))
                result = None
                sleep(self.sleep_time_long)
                self.__init_apis()
        return result


    def __secure_download(self, image_url, download_path, max_retry=2):
        for t in range(max_retry):
            try:
                self.aapi.download(image_url, download_path)
            except PixivError as error:
                self.log.debug(
                    "__secure_download {} {} {}".format(image_url, download_path, error))
                sleep(self.sleep_time_long)
                self.__init_apis()


    def download_all_works(self, artist_pixiv_id):
        """ Download all artworks from an artist

        Args:
            artist_pixiv_id (int): artist's id, which can be seen
                in their personal page.
        Returns:
            boolean: True if it downloaded all images.
        """

        pdb = self.__open_db()
        result = self.__secure_users_works(artist_pixiv_id)
        if not result is None and result.pagination.total > 0:
            total_works = result.pagination.total
        else:
            return True
        num_pages = result.pagination.pages
        illust = result.response[0]
        saving_directory_path = os.path.join(
            os.getcwd(), self.folder_name, str(artist_pixiv_id))
        PixivDownloader.make_dir(saving_directory_path)
        self.print_header(artist_pixiv_id, total_works)
        counter = TqdmCounter(tqdm_enabled=self.tqdm != None,
                              total=total_works,
                              desc="DL ({})".format(artist_pixiv_id))
        for page in range(1, num_pages+1):
            if page != 1:
                result = self.__secure_users_works(artist_pixiv_id, page)
                if result is None:
                    break
            pdb.insert_multiple(result.response)
            for work_no in range(result.count):
                if counter.isEnd():  #num_downloaded == total_works:
                    break
                illust = result.response[work_no]
                image_url = illust.image_urls.large
                file_name = os.path.splitext(os.path.basename(image_url))[0]
                extension = os.path.splitext(os.path.basename(image_url))[1]
                file_path = os.path.join(
                    saving_directory_path, "{}{}".format(file_name, extension))
                if not os.path.isfile(file_path):
                    self.print('URL: %s' % image_url)
                    self.print(file_path)
                    self.__secure_download(image_url, saving_directory_path+os.sep)
                    sleep(self.sleep_time)
                    self.__random_sleep()
                counter.update(1)
        counter.close()
        return True
